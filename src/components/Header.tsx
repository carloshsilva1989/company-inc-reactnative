import React, { useState, type PropsWithChildren } from 'react';
import {
    Text,
    TextInput,
    View,
} from 'react-native';
import tw from 'twrnc';

const Header = (PropsWithChildren:any) => {
    return (
        <View style={tw.style('shadow-inner sticky top-0 z-50')}>
            <Text style={tw.style('text-2xl text-center text-blue-800')}>
                Company INC
            </Text>
            <View style={tw.style('flex flex-row mx-2 my-2')}>
                <View style={tw.style('w-94 p-2')}>
                    <TextInput
                        style={tw.style('w-full p-4 border-solid shadow-inner-lg bg-gray-200 rounded-lg')}
                        placeholder="Buscar el coin!"
                        onChangeText={newText => PropsWithChildren.searchHandler(newText)}
                    />
                </View>
            </View>
        </View>
    );
};


export default Header;
