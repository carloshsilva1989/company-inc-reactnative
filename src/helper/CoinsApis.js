import axios from 'axios';

export const getAllCoins = () => {
    return new Promise((resolve, reject) => {
      axios({
        method: 'GET',
        url: 'https://api.coinlore.net/api/tickers/',
      })
        .then((res) => resolve(res.data))
        .catch((error) => reject(error.response));
    });
  };


