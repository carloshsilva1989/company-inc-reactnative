import React, { useEffect, useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    TouchableOpacity
} from 'react-native';
import tw from 'twrnc';
import Header from '../components/Header';
import { getAllCoins } from '../helper/CoinsApis'

import { Button } from "react-native";
import Modal from "react-native-modal";

const home = () => {
    const [dataList, setDatalist] = useState([])
    const [dataListSearch, setDatalistSearch] = useState([])
    const [isModalVisible, setModalVisible] = useState(false);
    const [itemSeleted, setItemSelected] = useState({})

    useEffect(() => {
        getCoins()
        //put interval to update list every minute
        const intervalId = setInterval(() => {
            getCoins()
        }, 60000)
        return () => clearInterval(intervalId);
    }, [])

    useEffect(() => {
        console.log('updating list')
    }, [dataList])

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };

    const getCoins = () => {
        console.log('Get Coins list')
        getAllCoins()
            .then((res) => {
                setDatalist(res.data)
                setDatalistSearch(res.data)
            })
            .catch((error) => console.log(error));
    }

    const UpDown = (item) => {
        if(item.percent_change_24h > 0)
        {
            return (
                <Text style={tw.style('text-green-400')}>Δ</Text>
            )
        }
        else 
        {
            return (
                <Text style={tw.style('text-red-400')}>∇</Text>
            )  
        }
    }

    const Item = ({ item }) => (
        <TouchableOpacity
            style={tw.style('bg-white-100 hover:bg-white-700 shadow-sm p-2 pt-4 inline-block align-middle pt-6')}
            onPress={() => {
                setItemSelected(item)
                setModalVisible(true)
            }}>
            <View style={tw.style('flex flex-row mx-2 my-2')}>
                <View style={tw.style('w-3/4')}>
                    <Text style={tw.style('text-md')}>{item.name}</Text>
                </View>
                <View style={tw.style('w-1/4')}>
                    <Text style={tw.style('text-md text-right')}>
                        <Text style={tw.style('text-green-600')}>$ </Text>
                        {item.price_usd} {UpDown(item)}
                        </Text>

                </View>
            </View>
            <View
                style={tw.style('pt-4', {
                    borderBottomColor: '#c3c3c3',
                    borderBottomWidth: StyleSheet.hairlineWidth,
                })}
            />
        </TouchableOpacity>
    );

    const renderItem = ({ item }) => (
        <Item item={item} />
    );

    const searchHandler = (text) => {
        setDatalistSearch(dataList.filter((item) => item.name.toLowerCase().match(new RegExp(text.toLowerCase(), 'g'))))
    }

    return (
        <>
            <Header searchHandler={searchHandler}></Header>
            <FlatList
                style={tw.style('h-full')}
                data={dataListSearch}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
            <View style={{ flex: 1 }}>
                <Button title="Show modal" onPress={toggleModal} />
                <Modal isVisible={isModalVisible}>
                    <View style={tw.style('bg-white flex h-auto rounded-lg py-8 px-8')} >
                        <Text style={tw.style('text-2xl text-blue-800')}>{itemSeleted.name}</Text>
                        <Text style={tw.style('text-lg')}>Symbol</Text>
                        <Text style={tw.style('text-gray-700')}>{itemSeleted.symbol}</Text>
                        <Text style={tw.style('text-lg')}>Name ID</Text>
                        <Text style={tw.style('text-gray-700')}>{itemSeleted.nameid}</Text>
                        <Text style={tw.style('text-lg')}>Rank</Text>
                        <Text style={tw.style('text-gray-700')}>{itemSeleted.nameid}</Text>
                        <Text style={tw.style('text-lg')}>Price usd</Text>
                        <Text style={tw.style('text-gray-700')}>{itemSeleted.price_usd}</Text>
                        <Text style={tw.style('text-lg')}>Percent change 24h</Text>
                        <Text style={tw.style('text-gray-700')}>{itemSeleted.percent_change_24h}</Text>
                        <Text style={tw.style('text-lg')}>Percent change 1h</Text>
                        <Text style={tw.style('text-gray-700')}>{itemSeleted.percent_change_1h}</Text>
                        <Text style={tw.style('text-lg')}>Percent change 7d</Text>
                        <Text style={tw.style('text-gray-700')}>{itemSeleted.percent_change_7d}</Text>
                        <TouchableOpacity style={tw.style('text-center rounded-lg bg-blue-800 w-full grid justify-items-center mt-8')} onPress={toggleModal}>
                            <Text style={tw.style('text-lg text-white text-center p-2')}>Cerrar</Text>
                        </TouchableOpacity>

                    </View>
                </Modal>
            </View>
        </>
    );
};

export default home;

