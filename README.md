
## Run Locally

Clone the project

```bash
  git clone git@gitlab.com:carloshsilva1989/company-inc-reactnative.git
```

Go to the project directory

```bash
  cd company-inc-reactnative
```

Install dependencies

```bash
  yarn install
```

Start app on IOS

```bash
  yarn run ios
```

Start app on Android

```bash
  yarn run android
```